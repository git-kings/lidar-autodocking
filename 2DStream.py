from ouster import client
from ouster import pcap
from contextlib import closing
import matplotlib.pyplot as plt
from more_itertools import nth
import numpy as np
import cv2
import open3d as o3d


hostname = '192.168.0.20' # Camera IP
# create empty config
config = client.SensorConfig()

# set the values that you need: see sensor documentation for param meanings
config.operating_mode = client.OperatingMode.OPERATING_NORMAL
config.lidar_mode = client.LidarMode.MODE_1024x10
config.udp_port_lidar = 7502
config.udp_port_imu = 7503

# set the config on sensor, using appropriate flags
client.set_config(hostname, config, persist=True, udp_dest_auto=True)

with closing(client.Sensor(hostname, 7502, 7503)) as source:
    # print some useful info from
    print("Retrieved metadata:")
    print(f"  serial no:        {source.metadata.sn}")
    print(f"  firmware version: {source.metadata.fw_rev}")
    print(f"  product line:     {source.metadata.prod_line}")
    print(f"  lidar mode:       {source.metadata.mode}")
    print(f"Writing to: {hostname}.json")

    # write metadata to disk
    source.write_metadata(f"{hostname}.json")

with closing(client.Scans.stream(hostname, config.udp_port_lidar,
                                 complete=False)) as stream:
    show = True
    while show:
        for scan in stream:
            # uncomment if you'd like to see frame id printed
            print("frame id: {} ".format(scan.frame_id))
            reflectivity = client.destagger(stream.metadata,
                                      scan.field(client.ChanField.REFLECTIVITY))
            reflectivity = (reflectivity / np.max(reflectivity) * 255).astype(np.uint8)
            cv2.imshow("scaled reflectivity", reflectivity)
            key = cv2.waitKey(1) & 0xFF
