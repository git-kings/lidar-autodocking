from ouster import client
from contextlib import closing
import numpy as np
import cv2 as cv
from statistics import mean, stdev
import socket
import json
import time

# Server configuration
host = '172.20.10.2'
port = 5555

# Create a socket object
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to a specific address and port
server_socket.bind((host, port))

# Listen for incoming connections
server_socket.listen()

print(f"Server listening on {host}:{port}")

# Accept a connection from a client
client_socket, client_address = server_socket.accept()
print(f"Connection from {client_address}")

# Initialize sensor parameters
hostname = "192.168.0.20"
lidar_port = 7502

# Determine number of frames to calculate average range
# num_frames = 10

# Determine area of interest (AOI = Area Of Interest)

# Areas should be directly frontfacing, left and right of the sensor

# Area 1
aoi_x = [64, 65]
aoi_y = [255, 256]
aoi_x_slice = slice(aoi_x[0], aoi_x[1])
aoi_y_slice = slice(aoi_y[0], aoi_y[1])
pixel_avg_left = 0
pixel_stdev = 0

# Establish mulitple areas of interest
# Area 2
aoi_x2 = [64, 65]
aoi_y2 = [511, 512]
aoi_x_slice2 = slice(aoi_x2[0], aoi_x2[1])
aoi_y_slice2 = slice(aoi_y2[0], aoi_y2[1])
pixel_avg2_middle = 0
pixel_stdev2 = 0

# Area 3
aoi_x3 = [64, 65]
aoi_y3 = [767, 768]
aoi_x_slice3 = slice(aoi_x3[0], aoi_x3[1])
aoi_y_slice3 = slice(aoi_y3[0], aoi_y3[1])
pixel_avg3_right = 0
pixel_stdev3 = 0

# Determine font parameters
font = cv.FONT_HERSHEY_SIMPLEX
bottomLeftCornerOfText = (aoi_y[0], aoi_x[0])
bottomLeftCornerOfText2 = (aoi_y2[0], aoi_x2[0])
bottomLeftCornerOfText3 = (aoi_y3[0], aoi_x3[0])
fontScale = .4
fontColor = (0, 255, 0)
lineType = 1

# Establish sensor connection
with closing(client.Scans.stream(hostname, lidar_port,
                                 complete=False)) as stream:
    show = True
    range_aoi_buffer = []
    range_aoi_buffer2 = []
    range_aoi_buffer3 = []

    while show:
        for scan in stream:
            # Uncomment if you'd like to see frame id printed
            # print("frame id: {} ".format(scan.frame_id))
            range = client.destagger(stream.metadata, scan.field(client.ChanField.RANGE))
            scaled_range = (range / np.max(range) * 255).astype(np.uint8)
            
            signal = client.destagger(stream.metadata,
                                      scan.field(client.ChanField.REFLECTIVITY))
        
            signal = (signal / np.max(signal) * 255).astype(np.uint8)


            # Define different areas for range averaging 
            range_aoi = range[aoi_x_slice, aoi_y_slice]
            range_aoi2 = range[aoi_x_slice2, aoi_y_slice2]
            range_aoi3 = range[aoi_x_slice3, aoi_y_slice3]

            # Establish processing for the first AOI
            # print (f*ranges_aoi = {range_aoi_buffer}")
            """range_aoi_buffer.append(int(range_aoi[0, 0]))
            range_aoi_buffer2.append(int(range_aoi2[0, 0]))
            range_aoi_buffer3.append(int(range_aoi3[0, 0]))
            
            if len(range_aoi_buffer) > num_frames:
                range_aoi_buffer.pop(0)
                # print(f" Last {num_frames} ranges for pixel {aoi_x[0], aoi_y[0]} = {range_aoi_buffer}")
                pixel_avg_left = mean(range_aoi_buffer)
                pixel_stdev = stdev(range_aoi_buffer)
                print(f"\nArea1: Pixel_Avg_Range = {pixel_avg_left}")
                #print(f"pixel_stdev = {pixel_stdev}")

            if len(range_aoi_buffer2) > num_frames:
                range_aoi_buffer2.pop(0)
                # print(f" Last {num_frames} ranges for pixel {aoi_x[0], aoi_y[0]} = {range_aoi_buffer}")
                pixel_avg2_middle = mean(range_aoi_buffer2)
                pixel_stdev = stdev(range_aoi_buffer2)
                print(f"\nArea2: Pixel_Avg_Range = {pixel_avg2_middle}")
                #print(f"pixel_stdev = {pixel_stdev}")

            if len(range_aoi_buffer3) > num_frames:
                range_aoi_buffer3.pop(0)
                # print(f" Last {num_frames} ranges for pixel {aoi_x[0], aoi_y[0]} = {range_aoi_buffer}")
                pixel_avg3_right = mean(range_aoi_buffer3)
                pixel_stdev = stdev(range_aoi_buffer3)
                print(f"\nArea3: Pixel_Avg_Range = {pixel_avg3_right}")
                #print(f"pixel_stdev = {pixel_stdev}")"""
            
            pixel_avg_left = int(range_aoi[0, 0])
            pixel_avg2_middle = int(range_aoi2[0, 0])
            pixel_avg3_right = int(range_aoi3[0, 0])

            sensorRanges = [pixel_avg_left, pixel_avg2_middle, pixel_avg3_right]

            # Establish vizualization for the AOIs                
            gray = np.concatenate((scaled_range, signal), axis=0)
            viz = cv.cvtColor(gray, cv.COLOR_GRAY2RGB)

            # Establish areas to be highlighted
            cv.rectangle(viz, (aoi_y[0], aoi_x[0]), (aoi_y[1]+2, aoi_x[1]+2), fontColor, -1)
            cv.rectangle(viz, (aoi_y2[0], aoi_x2[0]), (aoi_y2[1]+2, aoi_x2[1]+2), fontColor, -1)
            cv.rectangle(viz, (aoi_y3[0], aoi_x3[0]), (aoi_y3[1]+2, aoi_x3[1]+2), fontColor, -1)
            cv.putText(viz, f" Area 1 - Left", bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
            cv.putText(viz, f" Area 2 - Middle", bottomLeftCornerOfText2, font, fontScale, fontColor, lineType)
            cv.putText(viz, f" Area 3 - Right", bottomLeftCornerOfText3, font, fontScale, fontColor, lineType)
            # cv.putText(viz, f" Range Precision: {pixel_stdev}", bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
            # cv.putText(viz, f" Range mean: {pixel_avg}", bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
            # cv.putText(viz, f" last {num_frames} frames", bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
            cv.imshow("Scaled Range & Signal", viz)

            # Generate a list of changing values (e.g., using a sine function)
            values = sensorRanges
            """# Convert the list to a JSON string
            json_values = json.dumps(values)

            # Send the JSON string to the client"""
            values_string = ",".join(map(str, values))

            # Send the string to the client
            client_socket.send(values_string.encode())

            

            

            cv.waitKey(1) & 0xFF

