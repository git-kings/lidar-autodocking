#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.



# Create your objects here.
ev3 = EV3Brick()

# Initialize motors at ports B and C.
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Create a drive base using the initialized motors.
robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=104)
robot.settings(straight_speed=1000)
# Write your program here.

# Play a sound.
ev3.speaker.beep()

# Run the drive base forward indefinitely.
robot.straight(10000)

# Stop the motors when a button is pressed.
robot.stop()
