from functools import partial
from ouster.viz import SimpleViz, ScansAccumulator
from ouster.mapping.slam import KissBackend
from ouster import client
from ouster.pcap import Pcap
import ouster.osf as osf
from ouster.sdkx.parsing import default_scan_fields
from ouster.sdk.util import resolve_metadata
import ipaddress

pcap_path = 'HalC.pcap'

def load_source(source):
    if source.endswith('.pcap'):
        metadata = open(resolve_metadata(source), "r").read()
        info = client.SensorInfo(metadata)
        pcap = Pcap(source, info)
        fields = default_scan_fields(info.format.udp_profile_lidar, flags=True)
        scans = client.Scans(pcap, fields=fields)
    elif source.endswith('.osf'):
        scans = osf.Scans(source)
        info = scans.metadata
    elif source.endswith('.local') or ipaddress.ip_address(source):
        scans = client.Scans.stream(hostname=source, lidar_port=7502, complete=False, timeout=1)
        info = scans.metadata
    else:
        raise ValueError("Not a valid source type")

    return scans, info



scans, info = load_source(pcap_path)
slam = KissBackend(info, max_range=75, min_range=1, voxel_size=1.0)

scans_w_poses = map(partial(slam.update), scans)
scans_acc = ScansAccumulator(info,
                             accum_max_num=10,
                             accum_min_dist_num=1,
                             map_enabled=True,
                             map_select_ratio=0.01)

SimpleViz(info, scans_accum=scans_acc, rate=0.0).run(scans_w_poses)