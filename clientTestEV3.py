#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import socket
import json
import time

# Create your objects here.
ev3 = EV3Brick()

# Initialize motors at ports B and C.
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Create a drive base using the initialized motors.
robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=104)
robot.settings(straight_speed=1000)
# Write your program here.

# Play a sound.
ev3.speaker.beep()


# Client configuration
host = '172.20.10.2'
port = 5555

# Create a socket object
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to the server
client_socket.connect((host, port))
print(host)

# Receive and parse the list of values from the server
try:
    while True:
        data = client_socket.recv(1024)

        if not data:
            break

        # Decode the received data
        values_string = data.decode()
        
        values_integers = list(map(int, values_string.split(',')))

        rangeLeft = values_integers[0]
        rangeMiddle = values_integers[1]
        rangeRight = values_integers[2]
        print(values_integers)
        
        if rangeMiddle > 0:
            print('Driving Foward')
            robot.drive(100, 0)
        elif rangeMiddle < 600:
            if rangeLeft > rangeRight:
                print('Turning Left')
                robot.drive(50, -50)
            else:
                print('Turning Right')
                robot.drive(50, 50)
       
except KeyboardInterrupt:
    pass

finally:
    # Close the connection
    client_socket.close()