from ouster import client
from ouster import pcap

pcap_path = 'exampel.pcap'
metadata_path = 'exampel.json'

hostname = '192.168.0.20' # Camera IP
config = client.SensorConfig() # Class for configurations parameters
config.udp_port_lidar = 7502 # Port for lidar data Osuter data = (Range, Signal, Near-IR and reflectivity)
config.udp_port_imu = 7503 # Port for IMU data Inertial Measurement Unit 
config.operating_mode = client.OperatingMode.OPERATING_NORMAL # Put sensor into operating mode, Normal or Standby
client.set_config(hostname, config, persist=True, udp_dest_auto = True) # Set config

with open(metadata_path, 'r') as f:
    info = client.SensorInfo(f.read())

source = pcap.Pcap(pcap_path, info)