import ouster.viz as viz
import ouster.client as client

hostname = '192.168.0.20'
metadata_path = '192.168.0.20.json'

config = client.SensorConfig() # Class for configurations parameters
config.udp_port_lidar = 7502 # Port for lidar data Osuter data = (Range, Signal, Near-IR and reflectivity)
config.udp_port_imu = 7503 # Port for IMU data Inertial Measurement Unit 
config.operating_mode = client.OperatingMode.OPERATING_NORMAL # Put sensor into operating mode, Normal or Standby
client.set_config(hostname, config, persist=True, udp_dest_auto = True) # Set config

with open(metadata_path, 'r') as f:
    metadata = client.SensorInfo(f.read())


viz.LidarScanViz(metadata, viz=None, _img_aspect_ratio=0)