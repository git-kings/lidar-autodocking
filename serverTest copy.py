import socket
import json
import time
import math

# Server configuration
host = '172.20.10.2'
port = 5555

# Create a socket object
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to a specific address and port
server_socket.bind((host, port))

# Listen for incoming connections
server_socket.listen()

print(f"Server listening on {host}:{port}")

# Accept a connection from a client
client_socket, client_address = server_socket.accept()
print(f"Connection from {client_address}")

# Send a list of changing values to the client
try:
    while True:
        # Generate a list of changing values (e.g., using a sine function)
        values = [math.sin(time.time()), math.cos(time.time()), math.tan(time.time())]

        # Convert the list to a JSON string
        json_values = json.dumps(values)

        # Send the JSON string to the client
        client_socket.send(json_values.encode())

        # Simulate a delay between messages
        time.sleep(4)

except KeyboardInterrupt:
    pass

finally:
    # Close the connection
    client_socket.close()
    server_socket.close()