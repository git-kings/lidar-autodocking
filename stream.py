from ouster import client
from ouster import pcap
from contextlib import closing
import matplotlib.pyplot as plt
from more_itertools import nth
import numpy as np
import cv2

hostname = '192.168.0.20' # Camera IP
config = client.SensorConfig() # Class for configurations parameters
config.udp_port_lidar = 7502 # Port for lidar data Osuter data = (Range, Signal, Near-IR and reflectivity)
config.udp_port_imu = 7503 # Port for IMU data Inertial Measurement Unit 
config.operating_mode = client.OperatingMode.OPERATING_NORMAL # Put sensor into operating mode, Normal or Standby
client.set_config(hostname, config, persist=True, udp_dest_auto = True) # Set config


with closing(client.Scans.stream(hostname, config.udp_port_lidar,
                                 complete=False)) as stream:
    show = True
    while show:
        for scan in stream:
            # uncomment if you'd like to see frame id printed
            # print("frame id: {} ".format(scan.frame_id))
            reflectivity = client.destagger(stream.metadata,
                                      scan.field(client.ChanField.REFLECTIVITY))
            reflectivity = (reflectivity / np.max(reflectivity) * 255).astype(np.uint8)
            cv2.imshow("scaled reflectivity", reflectivity)
            key = cv2.waitKey(1) & 0xFF 