from ouster import client
from ouster import pcap
from contextlib import closing
import matplotlib.pyplot as plt
from more_itertools import nth
import math
import numpy as np

pcap_path = 'OS-1-128_v3.0.1_1024x10_20230216_142857-000.pcap' # Demofile pcap
metadata_path = 'OS-1-128_v3.0.1_1024x10_20230216_142857.json' # Demofile json

hostname = '192.168.0.20'

# create empty config
config = client.SensorConfig()

# set the values that you need: see sensor documentation for param meanings
config.operating_mode = client.OperatingMode.OPERATING_NORMAL
config.lidar_mode = client.LidarMode.MODE_1024x10
config.udp_port_lidar = 7502
config.udp_port_imu = 7503

# set the config on sensor, using appropriate flags
client.set_config(hostname, config, persist=True, udp_dest_auto=True)

config.udp_profile_lidar = client.UDPProfileLidar.PROFILE_LIDAR_RNG19_RFL8_SIG16_NIR16_DUAL # The configuration of the LIDAR data packets

with closing(client.Sensor(hostname, 7502, 7503)) as source:
    # print some useful info from
    print("Retrieved metadata:")
    print(f"  serial no:        {source.metadata.sn}")
    print(f"  firmware version: {source.metadata.fw_rev}")
    print(f"  product line:     {source.metadata.prod_line}")
    print(f"  lidar mode:       {source.metadata.mode}")
    print(f"Writing to: {hostname}.json")

    # write metadata to disk
    source.write_metadata(f"{hostname}.json") # Log metadata

with open(metadata_path, 'r') as f:
    metadata = client.SensorInfo(f.read())

source = pcap.Pcap(pcap_path, metadata)

"""for packet in source: # Read packets from source
    if isinstance(packet, client.LidarPacket):
        # Now we can process the LidarPacket. In this case, we access
        # the measurement ids, timestamps, and ranges
        measurement_ids = packet.measurement_id
        timestamps = packet.timestamp
        ranges = packet.field(client.ChanField.RANGE)
        print(f'  encoder counts = {measurement_ids.shape}')
        print(f'  timestamps = {timestamps.shape}')
        print(f'  ranges = {ranges.shape}')

    elif isinstance(packet, client.ImuPacket):
        # and access ImuPacket content
        print(f'  acceleration = {packet.accel}')
        print(f'  angular_velocity = {packet.angular_vel}')"""

# ... `metadata` and `source` variables created as in the previous examples

scans = client.Scans(source)

# iterate `scans` and get the 84th LidarScan (it can be different with your data)
scan = nth(scans, 200)
print(f'scan: {scan}')
ranges = scan.field(client.ChanField.RANGE)
print(f'ranges: {ranges}')

# destagger ranges, notice `metadata` use, that is needed to get
# sensor intrinsics and correctly data transforms
ranges_destaggered = client.destagger(source.metadata, ranges)

plt.imshow(ranges_destaggered, cmap='gray', resample=False)
plt.show()

# obtain destaggered range
range_destaggered = client.destagger(metadata,
                                     scan.field(client.ChanField.RANGE))

# obtain destaggered xyz representation
xyzlut = client.XYZLut(metadata)
xyz_destaggered = client.destagger(metadata, xyzlut(scan))
print(f'xyz_destaggered: {xyz_destaggered}')

# select only points with more than min range using the range data
xyz_filtered = xyz_destaggered * (range_destaggered[:, :, np.newaxis] >
                                  (1 * 1000))
print(f'xyz_filtered: {xyz_filtered}')

# get first 3/4 of scan
to_col = math.floor(metadata.mode.cols * 3 / 4)
xyz_filtered = xyz_filtered[:, 0:to_col, :]
print(f'xyz_filtered2: {xyz_filtered}')